/**
 * ***********************************************************************************************
 * @file    : hdc1080.c
 * @brief   : source file of hdc1080 sensor
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include -------------------------------------------------------------------------------------*/
#include "hdc1080.h"
#include "stm8l15x.h"
#include "usr_i2c.h"
#include "usr_delay.h"

/**
 * @brief   : init HD1080 Sensor
 * @param   : None
 * @return  : None
**/
void hdc1080_init(void){	
	//set thr cho tem va hu
	uint8_t debug[3] = {0x02, 0x10, 0};
	uint8_t Temp_oxOE = 0x50;
	uint8_t Temp_0xOF = 0x00;
    //i2c_master_regAddr_write(I2C1, HDC_1080_ADD_1<<1, RESET_DRDY_INT_CONF,1, &Temp_oxOE, 1);
	//HAL_I2C_Mem_Write(&hi2c1, HDC_1080_ADD_1<<1, RESET_DRDY_INT_CONF,I2C_MEMADD_SIZE_8BIT, &Temp_oxOE, 1,1000);
	//i2c_master_regAddr_write(I2C1, HDC_1080_ADD_1<<1, CONFIGRATION_REGISTER_ADD,1, &Temp_0xOF, 1);
	i2c_master_data_write(I2C1, HDC_1080_ADD_1<<1, debug, 3);
}


/**
 * @brief   : config resolution
 * @param1  :
 * @param2  :
 * @param3  :
 * @return  : None
**/
void hdc1080_Resolution(Temp_Reso Temperature_Resolution_x_bit,Humi_Reso Humidity_Resolution_x_bit)
{
	/* Temperature and Humidity are acquired in sequence, Temperature first
	 * Default:   Temperature resolution = 14 bit,
	 *            Humidity resolution = 14 bit
	 */

	/* Set the acquisition mode to measure both temperature and humidity by setting Bit[12] to 1 */
	uint8_t config_reg_value=0x01;

	switch(Temperature_Resolution_x_bit)  
	{
		case Temperature_Resolution_11_bit:
			config_reg_value |= 0x40;
			break;
		case Temperature_Resolution_8_bit:
			config_reg_value |= 0x80;
			break;
		default:
			break;
	}

	switch(Humidity_Resolution_x_bit)
	{
		case Humidity_Resolution_11_bit:
			config_reg_value |= 0x10;
			break;
		case Humidity_Resolution_8_bit:
			config_reg_value |= 0x20;
			break;
		default:
			break;
	}

    //i2c_master_regAddr_write(I2C1, HDC_1080_ADD_1<<1, CONFIGRATION_REGISTER_ADD,1, &config_reg_value, 1);
	//HAL_I2C_Mem_Write(hi2c_x, HDC_1080_ADD_1<<1, CONFIGRATION_REGISTER_ADD,I2C_MEMADD_SIZE_8BIT, &config_reg_value, 1,1000);
        uint8_t debug_send_config[2];
       debug_send_config[0] = CONFIGRATION_REGISTER_ADD;
       debug_send_config[1] = config_reg_value;
       i2c_master_data_write(I2C1, HDC_1080_ADD_1<<1, debug_send_config, 2);
}


/**
 * @brief   : start measurement
 * @param1  :
 * @param2  :
 * @param3  :
 * @return  : None
**/
float debug_data = 0;
	uint32_t temp_x = 0;
	uint32_t humi_x = 0;
uint8_t hdc1080_start_measurement(double* temperature, double* humidity){
	uint8_t receive_data[4];

	uint8_t send_data = 0;

	
	//HAL_I2C_Master_Transmit(hi2c_x, HDC_1080_ADD_1<<1,&send_data,1,1000);
    i2c_master_data_write(I2C1, HDC_1080_ADD_1<<1, &send_data, 1);

	/* Delay here 15ms for conversion compelete.
	 * Note: datasheet say maximum is 7ms, but when delay=7ms, the read value is not correct
	 */
    delay_ms(200);
	
	/* Read temperature and humidity */
	//HAL_I2C_Master_Receive(hi2c_x, HDC_1080_ADD_1<<1, receive_data,4,1000);
    i2c_master_data_read(I2C1, HDC_1080_ADD_1<<1, receive_data,4);

	temp_x =((uint16_t)receive_data[0]<<8)|(uint16_t)receive_data[1];
	humi_x =((uint16_t)receive_data[2]<<8)|(uint16_t)receive_data[3];

	*temperature=((temp_x/65536.0) * 165)-40;
	*humidity=(humi_x / 65536.0) * 100;

	return 0;

}


/**
 * @brief   : start measurement Humidity
 * @param1  :
 * @param2  :
 * @param3  :
 * @return  : None
**/
void hdc1080_Start_Mea_Humidity(uint16_t* humidity){
	uint8_t receive_data[2];
	uint32_t humi_x = 0;
	uint8_t send_data = HUMIDITY_REGISTER_ADD;
	
	//HAL_I2C_Master_Transmit(hi2c_x, HDC_1080_ADD_1<<1,&send_data,1,1000);
    i2c_master_data_write(I2C1, HDC_1080_ADD_1<<1,&send_data,1);
	/* Read temperature and humidity */
	//HAL_I2C_Master_Receive(hi2c_x, HDC_1080_ADD_1<<1, receive_data,2,1000);
	humi_x =((receive_data[1]<<8)|receive_data[0]);
	*humidity=(uint16_t) (humi_x * 1000/65536);
	
}

void hdc1080_Start_Mea_Temp(uint16_t* temperature)
{
	uint8_t receive_data[2];
	uint32_t temp_x = 0;
	uint8_t send_data = TEMPERATURE_REGISTER_ADD;
	
	//HAL_I2C_Master_Transmit(hi2c_x, HDC_1080_ADD_1<<1,&send_data,1,1000);
    i2c_master_data_write(I2C1, HDC_1080_ADD_1<<1,&send_data,1);
	/* Read temperature and humidity */
	//HAL_I2C_Master_Receive(hi2c_x, HDC_1080_ADD_1<<1, receive_data,2,1000);
	temp_x =((receive_data[1]<<8)|receive_data[0]);
	*temperature=(uint16_t) ((temp_x*1650)/65536)-400;    //d� nh�n 10 r�i
	
}
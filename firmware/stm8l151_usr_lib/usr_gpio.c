/**
 * ***********************************************************************************************
 * @file    : usr_gpio.c
 * @brief   : source file of gpio for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include -------------------------------------------------------------------------------------*/
#include "usr_gpio.h"


/* Function ------------------------------------------------------------------------------------*/

/**
 * @brief   : init gpio for STM8L
 * @param   : None
 * @return  : None
**/ 
void gpio_init(void){
    GPIO_DeInit(NSS_GPIO_PORT);
    GPIO_DeInit(LORA_RST_GPIO_PORT);
    GPIO_DeInit(LORA_PWR_GPIO_PORT);
    // output gpio
    GPIO_Init(NSS_GPIO_PORT, NSS_GPIO_PIN, GPIO_Mode_Out_PP_High_Fast);
    GPIO_Init(LORA_RST_GPIO_PORT, LORA_RST_GPIO_PIN, GPIO_Mode_Out_PP_High_Fast);
    GPIO_Init(LORA_PWR_GPIO_PORT, LORA_PWR_GPIO_PIN, GPIO_Mode_Out_PP_High_Fast);
    GPIO_Init(GPIOD, GPIO_Pin_0, GPIO_Mode_Out_PP_Low_Fast);
    GPIO_Init(GPIOD, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);

    
    GPIO_Init(GPIOB, GPIO_Pin_3, GPIO_Mode_Out_PP_Low_Fast);
    
    //GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_In_PU_No_IT);
    // input gpio
    GPIO_Init(LORA_DIO_O_GPIO_PORT, LORA_DIO_O_GPIO_PIN, GPIO_Mode_In_FL_No_IT);
    GPIO_Init(GPIOC, GPIO_Pin_0, GPIO_Mode_In_FL_No_IT);
    //GPIO_Init(GPIOC, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);
    //GPIO_Init(GPIOC, GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);
    
    //GPIO_Init(GPIOB, GPIO_Pin_7, GPIO_Mode_In_PU_No_IT);
}
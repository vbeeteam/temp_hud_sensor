/**
 * ***********************************************************************************************
 * @file    : usr_i2c.h
 * @brief   : header file of I2C for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef USR_I2C_H
#define USR_I2C_H

/* Include --------------------------------------------------------------------------------------*/
#include "stm8l15x.h"

/* Public Function ------------------------------------------------------------------------------*/
void i2c_init(void);
void i2c_master_regAddr_write(I2C_TypeDef *i2c_name, uint8_t dev_addr, uint16_t reg_addr, uint8_t reg_size, uint8_t *p_data, uint8_t data_size);
void i2c_master_data_write(I2C_TypeDef *i2c_name, uint8_t dev_addr, uint8_t *p_data, uint8_t data_size);
void i2c_master_data_read(I2C_TypeDef *i2c_name, uint8_t dev_addr, uint8_t *p_data, uint8_t data_size);

#endif

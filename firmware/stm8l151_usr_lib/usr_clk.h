/**
 * ***********************************************************************************************
 * @file    : usr_clk.h
 * @brief   : header file of config clock for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef USR_CLK_H
#define USR_CLK_H

/* Include --------------------------------------------------------------------------------------*/

/* Public Funciton ------------------------------------------------------------------------------*/
void clk_config(void);

#endif

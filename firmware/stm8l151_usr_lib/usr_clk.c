/**
 * ***********************************************************************************************
 * @file    : usr_clk.c
 * @brief   : source file of config clock for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include --------------------------------------------------------------------------------------*/
#include "usr_clk.h"
#include "stm8l15x.h"

/* Function -------------------------------------------------------------------------------------*/
/*
 * @brief   : config clock for STM8L: HSE, Prescaler = 8
 * @param   : None
 * @return  : None
*/
void clk_config(void){
    /* Select HSE as system clock source */
    CLK_SYSCLKSourceSwitchCmd(ENABLE);
    CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSE);
    /* system clock prescaler: 8*/
    CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
    while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSE){
        ;
    }
}
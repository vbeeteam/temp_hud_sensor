/**
 * ***********************************************************************************************
 * @file    : usr_clk.c
 * @brief   : source file of spi for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include -------------------------------------------------------------------------------------*/
#include "usr_spi.h"
#include "usr_delay.h"

/* Function ------------------------------------------------------------------------------------*/

/*
 * @brief   : init SPI1 for STM8L: 
 * @param   : None
 * @return  : None
*/
void spi_init(void){
    /* Enable SPI clock */
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1,ENABLE);
    /* Set the MOSI,MISO and SCK at high level */
    GPIO_ExternalPullUpConfig(GPIOB, GPIO_Pin_5 | \
                            GPIO_Pin_6 | GPIO_Pin_7, ENABLE);
    /* SPI configuration */
SPI_DeInit(SPI1);
    SPI_Init(SPI1,SPI_FirstBit_MSB, 
             SPI_BaudRatePrescaler_2, SPI_Mode_Master,
             SPI_CPOL_Low, SPI_CPHA_1Edge, SPI_Direction_2Lines_FullDuplex,
             SPI_NSS_Soft, 0x07);
    /* Enable SPI  */
    SPI_Cmd(SPI1, ENABLE);
}

/*
 * @brief   : SPI Transmit and Receive: 
 * @param1  : SPI typedef
 * @param2  : pointer of tx buffer
 * @param3  : pointer of rx buffer
 * @param4  : len of buffer
 * @return  : true/false
*/
bool spi_transmit_recv(SPI_TypeDef *spi, uint8_t *tx_buff, uint8_t *rx_buff, uint8_t len){
    uint16_t spi_timeout = 2000;
    /* Wait until the transmit buffer is empty */
    while ((SPI_GetFlagStatus(spi, SPI_FLAG_TXE) == RESET) && (spi_timeout > 0)){
        spi_timeout--;
        delay_ms(1);
    }
    if(spi_timeout == 0){
        return false;
    }
    /* transmit and recv data */
    for (uint16_t i = 0; i < len; i++){
        SPI_SendData(spi, tx_buff[i]);
        spi_timeout = 5000;
        while ((SPI_GetFlagStatus(spi, SPI_FLAG_RXNE) == RESET)&&(spi_timeout > 0)){
            spi_timeout--;
        }
        if(spi_timeout == 0){
            return false;
        }
        rx_buff[i] = SPI_ReceiveData(spi);        
    }
    return true;
}

/*
 * @brief   : SPI Transmit 
 * @param1  : SPI typedef
 * @param2  : pointer of tx buffer
 * @param3  : len of buffer
 * @return  : true/false
*/
bool spi_transmit(SPI_TypeDef *spi, uint8_t *tx_buff, uint8_t len){
    uint16_t spi_timeout = 2000;
    /* Wait until the transmit buffer is empty */
    while ((SPI_GetFlagStatus(spi, SPI_FLAG_TXE) == RESET) && (spi_timeout > 0)){
        spi_timeout--;
        delay_ms(1);
    }
    if(spi_timeout == 0){
        return false;
    }
    /* transmit data */
    for (uint16_t i = 0; i < len; i++){
        SPI_SendData(spi, tx_buff[i]);
        spi_timeout = 5000;
        while ((SPI_GetFlagStatus(spi, SPI_FLAG_TXE) == RESET)&&(spi_timeout > 0)){
            spi_timeout--;
        }
        if(spi_timeout == 0){
            return false;
        }
    }
    return true;
}
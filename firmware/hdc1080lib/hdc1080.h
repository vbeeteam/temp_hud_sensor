/**
 * ***********************************************************************************************
 * @file    : hdc1080.h
 * @brief   : header file of hdc1080 sensor
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef HDC1080_H
#define HDC1080_H

/* Include -------------------------------------------------------------------------------------*/
#include <stdint.h>
#include "usr_i2c.h"

/* Definition ----------------------------------------------------------------------------------*/
#define HDC_1080_ADD_1                          0x40    //ADD to GND
#define CONFIGRATION_REGISTER_ADD               0x0F
#define TEMPERATURE_REGISTER_ADD                0x00
#define HUMIDITY_REGISTER_ADD                   0x02

#define STATUS_EXT	                            0x04
#define TEMPERATURE_MAX             			0x05
#define HUMIDITY_MAX              			    0x06
#define INTERUP_ENABLE                          0x07

#define TEMP_OFFSET_ADJUST        			    0x08
#define HUM_OFFSET_ADJUST                       0x09

#define TEMP_THR                                0x0A
#define RH_THR            						0x0C
#define RESET_DRDY_INT_CONF              	    0x0E
#define MANUFACTURER_ID_LOW                     0xFC

#define DEVICE_ID_LOW                           0xFE

/* Structure ----------------------------------------------------------------------------------*/
typedef enum
{
  Temperature_Resolution_14_bit = 0,
  Temperature_Resolution_11_bit = 1,
  Temperature_Resolution_8_bit = 2,
}Temp_Reso;

typedef enum
{
  Humidity_Resolution_14_bit = 0,
  Humidity_Resolution_11_bit = 1,
  Humidity_Resolution_8_bit =2,
}Humi_Reso;


/* Public Function -----------------------------------------------------------------------------*/
void hdc1080_init(void);
void hdc1080_Resolution(Temp_Reso Temperature_Resolution_x_bit,Humi_Reso Humidity_Resolution_x_bit);
uint8_t hdc1080_start_measurement(double* temperature, double* humidity);
void hdc1080_Start_Mea_Humidity(uint16_t* humidity);
void hdc1080_Start_Mea_Temp(uint16_t* temperature);

#endif

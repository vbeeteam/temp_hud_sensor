/**
 * ***********************************************************************************************
 * @file    : dev_service.h
 * @brief   : header file of service for device
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef DEV_SERVICE_H
#define DEV_SERVICE_H

/* Include -------------------------------------------------------------------------------------*/
#include "stm8l15x.h"
#include "stdbool.h"
#include "sx1276_78-LoRa.h"

/* Definition ----------------------------------------------------------------------------------*/

typedef enum{
    VB_REQUEST_SCHEDULE_SERV      = 99,
    VB_REPORT_DATA_SERV             = 30,
} VbeeServTypeDef;

extern tLoRaSettings LoRaSettings;

/* public Function -----------------------------------------------------------------------------*/
bool vbee_net_init(void);


#endif

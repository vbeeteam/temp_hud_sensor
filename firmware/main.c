/**
  ******************************************************************************
  * @file    Project/STM8L15x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V1.6.1
  * @date    30-September-2014
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x.h"

#include "usr_clk.h"
#include "usr_delay.h"
#include "usr_spi.h"
#include "usr_i2c.h"
#include "usr_gpio.h"
#include "usr_adc.h"

#include "hdc1080.h"

#include <stdio.h>

#include "dev_process.h"
#include "dev_service.h"

#include "board.h"
#include "string.h"
#include "radio.h"
#include "sx1276_78-LoRa.h"
#include "sx1276_78-LoRaMisc.h"
#include "platform.h"

/** @addtogroup STM8L15x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

uint8_t debug_send[14]={0x68, 0x28, 0x62, 0x06, 0x15, 0x20, 0x00, 0x68, 0x01, 0x02, 0x52, 0xC3, 0xAD, 0x16};
uint8_t flag_send=0;
uint8_t data_recv[100];
uint8_t flag_recv=0;

uint8_t debug_count=0;

uint8_t count_node_id[4] = {1, 2, 3, 4};

int debug = 0;

double temp_value = 0;
double humidity_value = 0;
float battery_value = 0;
uint8_t battery_per = 100;

uint32_t start_time = 0;
uint16_t period_time = 5000;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define BUFFER_SIZE                                 150 // Define the payload size here
#define newline 				"\r\n"

static uint16_t BufferSize = BUFFER_SIZE;			// RF buffer size
static uint8_t Buffer[BUFFER_SIZE];					// RF buffer


char authen[] = "DC*k6kg3d*10*5*16*0000000000000000";
uint8_t authen_len = 34;
//char dev_serial[6] = "5XZCAf";
char dev_serial[6] = "5XZCAf";

volatile char *data_send = "DC*5XZCAf*31*5*72*";
uint8_t body_len = 0;
char body_data[] = "[{\"type\":\"temperature\",\"value\":31.19},{\"type\":\"humidity\",\"value\":75.02}]";
char body_data_1[] = "[{\"type\":\"temperature\",\"value\":";
char body_data_2[] = "},{\"type\":\"humidity\",\"value\":";
char body_data_3[] = "}]";

extern uint32_t time_tx_value;

uint8_t count = 0;

void Lora_Tx_Data(void);
bool Lora_Rx(uint8_t *data);
void myPrintf(float fVal, char *result);
void floatToBytes(char * result, float flt);


extern tRadioDriver *Radio;

/*
void myPrintf(float fVal, char *result)
{
    int dVal, dec, i;

    fVal += 0.005;   // added after a comment from Matt McNabb, see below.

    dVal = fVal;
    dec = (int)(fVal * 100) % 100;

    result[0] = (dec % 10) + '0';
    result[1] = (dec / 10) + '0';
    result[2] = '.';

    i = 3;
    while (dVal > 0)
    {
        result[i] = (dVal % 10) + '0';
        dVal /= 10;
        i++;
    }
}
*/

void floatToBytes(char * result, float flt)
{
  uint8_t byte[2];
  byte[0] = (uint8_t) flt;    //truncate whole numbers
  flt = (flt - byte[0])*100; //remove whole part of flt and shift 2 places over
  byte[1] = (uint8_t) flt;    //truncate the fractional part from the new "whole" part
  
  result[0] = byte[0] / 10 + 48;
  result[1] = byte[0] % 10 + 48;
  result[2] = '.';
  result[3] = byte[1] / 10 + 48;
  result[4] = byte[1] % 10 + 48;
  result[5] = 0;
}


void Lora_Tx_Data(void){
  uint8_t i;
	for( i = 0; i < 18; i++ )
	{
		Buffer[i] = data_send[i];
	}	
        
        for( i = 0; i < strlen(body_data_1); i++ )
	{
			Buffer[i+18] = body_data_1[i];
	}
        body_len = strlen(body_data_1) + 18;
        char convert_data[6];
        
        floatToBytes(convert_data, temp_value);
        
        for(i=0; i<5; i++){
          Buffer[i+body_len] = convert_data[i];
        }
        
        body_len += 5;
        
        
        
	for( i = 0; i < strlen(body_data_2); i++ )
	{
          Buffer[i+body_len] = body_data_2[i];
	}    
        
        body_len += strlen(body_data_2);
        
         floatToBytes(convert_data, humidity_value);
        for(i=0; i < 5; i++ )
	{
          Buffer[i+body_len] = convert_data[i];
	}
        body_len += 5;
        
	for( i = 0; i < strlen(body_data_3); i++ )
	{
          Buffer[i+body_len] = body_data_3[i];
	}    
        
        body_len += strlen(body_data_3);
        
        
  LoRaSettings.SpreadingFactor = 7;
  SX1276LoRaSetSpreadingFactor( LoRaSettings.SpreadingFactor );
	Radio->SetTxPacket( Buffer, body_len );
	while(Radio->Process() != RF_TX_DONE);
  LoRaSettings.SpreadingFactor = 8;
  SX1276LoRaSetSpreadingFactor( LoRaSettings.SpreadingFactor );  
  Radio->StartRx();
}

uint8_t debug_1 = 0;
bool Lora_Rx(uint8_t *data)
{
  switch(Radio->Process())
  {
    case RF_RX_DONE:
      Radio->GetRxPacket(data, ( uint16_t* )&BufferSize );
      flag_recv=1;
      debug_1++;
      //GPIO_ToggleBits(GPIOB, GPIO_Pin_3);
      return true;
  }
  return false;
}


/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */

void main(void)
{
	/* Infinite loop */
	clk_config();
	gpio_init();
	spi_init();
	i2c_init();
	delay_init();
        TIM2_Config();        
	adc_init();


	//BoardInit();
        

	//hdc1080_init();
	//GPIO_SetBits(GPIOB, GPIO_Pin_3);
	//hdc1080_Resolution(Temperature_Resolution_14_bit,Humidity_Resolution_14_bit);

	dev_process_init();
        
  //LoRaSettings.RFFrequency = 433000000;
  //SX1276LoRaSetRFFrequency( LoRaSettings.RFFrequency );
  //LoRaSettings.SpreadingFactor = 8;
  //SX1276LoRaSetSpreadingFactor(LoRaSettings.SpreadingFactor);
  //Radio->StartRx();
        //Lora_Tx_Data();
    //debug_1 = GPIO_ReadInputDataBit( DIO0_IOPORT, DIO0_PIN );
    //uint8_t i=0;
    //i++;
    //authen[0] = '1';
    //flag_recv = 9;
	  //GPIO_ResetBits(GPIOB, GPIO_Pin_3);
    //serv_req_schedule(1000);
    
    
		
	  //delay_ms(6000);
    //LoRaSettings.RFFrequency = 434000000;
    //SX1276LoRaSetRFFrequency( LoRaSettings.RFFrequency );    
    //Lora_Tx_data();
    //GPIO_ToggleBits(GPIOB, GPIO_Pin_3);
    //debug_1++;
    //LoRaSettings.RFFrequency = 433000000;
    //SX1276LoRaSetRFFrequency( LoRaSettings.RFFrequency );
    //delay_ms(5000);
    //Lora_Tx_Data();
    

    //GPIO_ToggleBits(GPIOB, GPIO_Pin_3);


    while (1){
      
      if(time_tx_value == 0){
        delay_ms_tx(4800);
        hdc1080_start_measurement(&temp_value,&humidity_value);
        Lora_Tx_Data();        
      }
      if(Lora_Rx(data_recv) == true){
        // check magic
        bool result = true;
        if((data_recv[0] == 'D') && (data_recv[1] == 'C')){
          // check serial
          result = true;
          for (uint8_t i = 0; i < 6; i++)
          {
            if(data_recv[i+3] != dev_serial[i]){
              result = false;
            }
          }
          if(result == true){
            // check service type
            if((data_recv[10] == '9')&&(data_recv[11] == '9')){
              // get start time
              start_time = (uint32_t)data_recv[17] | (uint32_t)data_recv[18] << 8 | (uint32_t)data_recv[19] << 16 | (uint32_t)data_recv[20] <<24;
              delay_ms_tx(start_time);
              while(time_tx_value);
              delay_ms_tx(4800);
              Lora_Tx_Data();
              
              
            }
          }
          
        }
      }
    }

    //delay_ms(period_time-3000);
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/**
 * ***********************************************************************************************
 * @file    : usr_gpio.h
 * @brief   : header file of gpio for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef USR_GPIO_H
#define USR_GPIO_H

/* Include -------------------------------------------------------------------------------------*/
#include "stm8l15x.h"

/* Definition ---------------------------------------------------------------------------------*/
#define NSS_GPIO_PORT           GPIOB
#define NSS_GPIO_PIN            GPIO_Pin_4

#define LORA_RST_GPIO_PORT      GPIOD
#define LORA_RST_GPIO_PIN       GPIO_Pin_4

#define LORA_DIO_O_GPIO_PORT    GPIOD
#define LORA_DIO_O_GPIO_PIN     GPIO_Pin_5

#define LORA_PWR_GPIO_PORT      GPIOD
#define LORA_PWR_GPIO_PIN       GPIO_Pin_3
/* Public Function -----------------------------------------------------------------------------*/
void gpio_init(void);

#endif

/**
 * ***********************************************************************************************
 * @file    : usr_lowpower.h
 * @brief   : header file of User lib for low power mode
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef USR_LOWPOWER_H
#define USR_LOWPOWER_H

/* include --------------------------------------------------------------------------------------*/
#include "stm8l15x.h"

/* public variable ------------------------------------------------------------------------------*/


/* public Function ------------------------------------------------------------------------------*/
void lowpower_init(void);
void lowpower_active(uint16_t time);
void lowpower_exit(void);

#endif

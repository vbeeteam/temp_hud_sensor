/**
 * ***********************************************************************************************
 * @file    : usr_delay.c
 * @brief   : source file of delay for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include --------------------------------------------------------------------------------------*/
#include "usr_delay.h"

/* Variable ------------------------------------------------------------------------------------*/
volatile uint32_t time_value = 0;
volatile uint32_t time_tx_value = 0;

/* Function -------------------------------------------------------------------------------------*/

/*
 * @brief   : init delay for STM8L: use timer 4, perios = 1ms
 * @param   : None
 * @return  : None
*/
void delay_init(void){
    /* Enable TIM4 CLK */
    CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
    TIM4_TimeBaseInit(TIM4_Prescaler_32,249); //1ms with HSE clock = 8MHz / 8
	TIM4_ClearFlag(TIM4_FLAG_Update);
	TIM4_ITConfig(TIM4_IT_Update,ENABLE);

	enableInterrupts();
	TIM4_Cmd(DISABLE);    
}

void TIM2_Config(void)
{
  /* TIM2 configuration:
     - TIM2 ETR is mapped to LSE
     - TIM2 counter is clocked by LSE div 4
      so the TIM2 counter clock used is LSE / 4 = 32.768 / 4 = 8.192 KHz
    TIM2 Channel1 output frequency = TIM2CLK / (TIM2 Prescaler * (TIM2_PERIOD + 1))
                                   = 8192 / (1 * 8) = 1024 Hz */
  /* Time Base configuration */
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
  TIM2_TimeBaseInit(TIM2_Prescaler_32, TIM2_CounterMode_Up, 249);

  /* Channel 1 configuration in PWM1 mode */
  /* TIM2 channel Duty cycle is 100 * TIM2_PULSE / (TIM2_PERIOD + 1) = 100 * 4/8 = 50 % */
	TIM2_ClearFlag(TIM2_FLAG_Update);
	TIM2_ITConfig(TIM2_IT_Update,ENABLE);
  /* TIM2 Main Output Enable */

  /* TIM2 counter enable */
        enableInterrupts();
  TIM2_Cmd(DISABLE);
}

/*
 * @brief   : interrupt for timer4
 * @param   : None
 * @return  : None
*/
void delay_isr(void){
	if(TIM4_GetITStatus(TIM4_IT_Update)==SET){
		if(time_tx_value!=0){
			time_tx_value--;
		}
		else{
			/* Disable Timer 4*/
			TIM4_Cmd(DISABLE);
		}
		TIM4_ClearITPendingBit(TIM4_IT_Update);
	}
}



void delay_isr2(void){
	if(TIM2_GetITStatus(TIM2_IT_Update)==SET){
		if(time_value!=0){
			time_value--;
		}
		else{
			/* Disable Timer 4*/
			TIM2_Cmd(DISABLE);
		}
		TIM2_ClearITPendingBit(TIM2_IT_Update);
	}
}

void delay_ms(uint32_t value){
    time_value = value;
	/* Reset Counter Register value  to zero*/
	TIM2->CNTRH = (uint8_t)(0);
        TIM2->CNTRL = 0;

	/* Enable Timer 4*/
	TIM2_Cmd(ENABLE);

	while(time_value);    
}
/*
 * @brief   : delay_ms
 * @param   : value of ms
 * @return  : None
*/
void delay_ms_tx(uint32_t value){
    time_tx_value = value;
	/* Reset Counter Register value  to zero*/
	TIM4->CNTR = (uint8_t)(0);

	/* Enable Timer 4*/
	TIM4_Cmd(ENABLE);
}
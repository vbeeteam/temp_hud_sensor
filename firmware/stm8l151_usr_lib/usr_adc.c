/**
 * ***********************************************************************************************
 * @file    : usr_adc.c
 * @brief   : source file of User lib for ADC
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include --------------------------------------------------------------------------------------*/
#include "usr_adc.h"
#include "stm8l15x.h"

/* Function -------------------------------------------------------------------------------------*/
/**
 * @brief   : init ADC
 * @param   : None
 * @return  : None
**/
void adc_init(void){
  /* Enable ADC1 clock */
  CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE);    
  /* Initialise and configure ADC1 */
  ADC_Init(ADC1, ADC_ConversionMode_Continuous, ADC_Resolution_12Bit, ADC_Prescaler_2);
  ADC_SamplingTimeConfig(ADC1, ADC_Group_SlowChannels, ADC_SamplingTime_384Cycles);

  /* Enable ADC1 */
  ADC_Cmd(ADC1, ENABLE);

  /* Enable ADC1 Channel 3 */
  ADC_ChannelCmd(ADC1, ADC_Channel_0, ENABLE);

  /* Enable End of conversion ADC1 Interrupt */
  ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);

  /* Start ADC1 Conversion using Software trigger*/
  ADC_SoftwareStartConv(ADC1);
}

uint16_t adc_getValue(void){
    uint16_t tmpreg = 0;

    /* Get last ADC converted data.*/
    tmpreg = (uint16_t)(ADC1->DRH);
    tmpreg = (uint16_t)((uint16_t)((uint16_t)tmpreg << 8) | ADC1->DRL);

    /* Return the selected ADC conversion value */
    return (uint16_t)(tmpreg) ;
}
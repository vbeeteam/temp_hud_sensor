/**
 * ***********************************************************************************************
 * @file    : data_struct.h
 * @brief   : header file of data struct
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef DATA_STRUCT_H
#define DATA_STRUCT_H
/* Include --------------------------------------------------------------------------------------*/
#include "stm8l15x.h"


/* definition typedef struct ----------------------------------------------------------------------*/

/**
 * @brief   : Vbee packet
**/
#define VBEE_PACKET_BODY_MAX            255
typedef struct{
    uint16_t magic;
    char node_serial[6];
    uint16_t serv_type;
    uint16_t dev_type;
    uint16_t body_len;
    uint8_t body[VBEE_PACKET_BODY_MAX];    
} VBeeDataStructTypeDef;


/**
 * @brief   : device information
**/
typedef struct{
    char dev_serial[6];
    uint16_t dev_type;
    uint32_t op_period;
    uint16_t time_start;
} DevInforTypeDef;


#endif

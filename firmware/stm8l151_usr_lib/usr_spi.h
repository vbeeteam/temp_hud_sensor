/**
 * ***********************************************************************************************
 * @file    : usr_clk.h
 * @brief   : header file of spi for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef USR_SPI_H
#define USR_SPI_H

/* Include -------------------------------------------------------------------------------------*/
#include "stm8l15x.h"
#include <stdbool.h>

/* Public Function -----------------------------------------------------------------------------*/
void spi_init(void);
bool spi_transmit_recv(SPI_TypeDef *spi, uint8_t *tx_buff, uint8_t *rx_buff, uint8_t len);
bool spi_transmit(SPI_TypeDef *spi, uint8_t *tx_buff, uint8_t len);

#endif

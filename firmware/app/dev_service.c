/**
 * ***********************************************************************************************
 * @file    : dev_service.c
 * @brief   : source file of service for device
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include --------------------------------------------------------------------------------------*/
#include "dev_service.h"
#include "data_struct.h"
#include "dev_process.h"
#include "usr_delay.h"

#include "board.h"
#include "string.h"
#include "radio.h"

#include "sx1276_78-LoRaMisc.h"
#include "platform.h"

/* Definition ----------------------------------------------------------------------------------*/
#define TIME_LORA_RECV             1000 // seconds


/* Variable -------------------------------------------------------------------------------------*/

// lora
tRadioDriver *Radio = NULL;

/* Function -------------------------------------------------------------------------------------*/

/**
 * @brief   : init vbee network
 * @param   : none
 * @return  : True/False
**/
bool vbee_net_init(void){
    Radio = RadioDriverInit();
	Radio->Init();
	Radio->StartRx();
    return true;
}

/**
 * @brief   : service for request time schedule in Lora network
 * @param   : 
 * @brief   : True/False
**/

/**
 * @brief   : service for report Data
 * @param   : 
 * @brief   : True/False
**/


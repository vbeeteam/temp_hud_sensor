/**
 * ***********************************************************************************************
 * @file    : usr_i2c.c
 * @brief   : source file of I2C for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/

/* Include --------------------------------------------------------------------------------------*/
#include "usr_i2c.h"
#include "usr_delay.h"

/* Function -------------------------------------------------------------------------------------*/

/**
 * @brief   : init I2C1 for STM8L
 * @param   : None
 * @return  : None
**/
void i2c_init(void){
    /* Deinit I2C */
      //GPIO_ExternalPullUpConfig(GPIOC, GPIO_Pin_0 | GPIO_Pin_1, ENABLE);
    I2C_DeInit(I2C1);
    /* I2C Periph clock enable */
    CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
    /* Enable I2C */
    I2C_Cmd(I2C1 ,ENABLE);
    /* I2C Configuration */
    I2C_Init(I2C1, 100000, 0x01, I2C_Mode_I2C, I2C_DutyCycle_2, I2C_Ack_Enable, I2C_AcknowledgedAddress_7bit);
    //I2C_ITConfig(I2C1, I2C_IT_ERR, ENABLE);
    //I2C_Cmd(I2C1 ,ENABLE);
}

/**
  * @brief      : Write to the specified register of device
  * @param1     : I2C typedef
  * @param2     : Dev address
  * @param3     : Reg address
  * @param4     : size of Reg address
  * @param5     : pointer of data
  * @param6     : size of data
  * @return     : None
  */
void i2c_master_regAddr_write(I2C_TypeDef *i2c_name, uint8_t dev_addr, uint16_t reg_addr, uint8_t reg_size, uint8_t *p_data, uint8_t data_size){
    /*-------------------------------- Transmission Phase -----------------------*/
  while(I2C_GetFlagStatus(i2c_name, I2C_FLAG_BUSY));
    /* Send START condition */
    I2C_GenerateSTART(i2c_name, ENABLE);

    /* Test on EV5 and clear it */
    while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_MODE_SELECT))  /* EV5 */{
    }

    /* Send dev slave address for write */
    I2C_Send7bitAddress(i2c_name, dev_addr, I2C_Direction_Transmitter);

    /* Test on dev EV6 and clear it */
    while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) /* EV6 */{
    }

    /* Send the specified register data pointer */
    for (uint8_t i = 0; i < reg_size; i++){
        I2C_SendData(i2c_name, (uint8_t)(reg_addr >> (8*(reg_size-1 - i))));
        /* Test on LM75_I2C EV8 and clear it */
        while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) /* EV8 */{
        }
    }
  
    /* Test on EV8 and clear it */
    while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) /* EV8 */{
    }

    /* Send LM75_I2C data */
    for (uint8_t i = 0; i < data_size; i++){
        I2C_SendData(i2c_name, (uint8_t)(p_data[i] >> (8*(data_size-1 - i))));
        /* Test on LM75_I2C EV8 and clear it */
        while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) /* EV8 */{
        }
    } 

    /* Send STOP Condition */
    I2C_GenerateSTOP(i2c_name, ENABLE);
}

/**
  * @brief      : Write data device
  * @param1     : I2C typedef
  * @param2     : Dev address
  * @param3     : pointer of data
  * @param4     : size of data
  * @return     : None
  */
 void i2c_master_data_write(I2C_TypeDef *i2c_name, uint8_t dev_addr, uint8_t *p_data, uint8_t data_size){
    /*-------------------------------- Transmission Phase -----------------------*/
   while(I2C_GetFlagStatus(i2c_name, I2C_FLAG_BUSY));
    /* Send START condition */
    I2C_GenerateSTART(i2c_name, ENABLE);

    /* Test on EV5 and clear it */
    while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_MODE_SELECT))  /* EV5 */{
    }

    /* Send dev slave address for write */
    I2C_Send7bitAddress(i2c_name, dev_addr, I2C_Direction_Transmitter);

    /* Test on dev EV6 and clear it */
    while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) /* EV6 */{
    }

    /* Send data */
    for (uint8_t i = 0; i < data_size; i++){
        //I2C_SendData(i2c_name, (uint8_t)(p_data[i] >> (8*(data_size-1 - i))));
        I2C_SendData(i2c_name, p_data[i]);
       /* Test on LM75_I2C EV8 and clear it */
       /* Test on LM75_I2C EV8 and clear it */
        while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) /* EV8 */
        {
        }
    } 

    /* Send STOP Condition */
    I2C_GenerateSTOP(i2c_name, ENABLE);
}


/**
  * @brief      : Write data device
  * @param1     : I2C typedef
  * @param2     : Dev address
  * @param3     : pointer of data
  * @param4     : size of data
  * @return     : None
  */
 void i2c_master_data_read(I2C_TypeDef *i2c_name, uint8_t dev_addr, uint8_t *p_data, uint8_t data_size){
    /*-------------------------------- Transmission Phase -----------------------*/
   while(I2C_GetFlagStatus(i2c_name, I2C_FLAG_BUSY));
    /* Send START condition */
    I2C_GenerateSTART(i2c_name, ENABLE);

    /* Test on EV5 and clear it */
    while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_MODE_SELECT))  /* EV5 */{
    }

    /* Send dev slave address for write */
    I2C_Send7bitAddress(i2c_name, dev_addr, I2C_Direction_Receiver);

    /* Test on dev EV6 and clear it */
    while (!I2C_CheckEvent(i2c_name, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) /* EV6 */{
    }

    /* read data */
    uint8_t i = 0;
    for (i = 0; i < data_size; i++){
         while (!I2C_GetFlagStatus(i2c_name, I2C_FLAG_RXNE)) /* EV8 */
        {
        }
        p_data[i] = I2C_ReceiveData(i2c_name);
        if(i == data_size - 4){
          break;
        }
    }
    
         while (!I2C_GetFlagStatus(i2c_name, I2C_FLAG_RXNE)) /* EV8 */
        {
        }
    //I2C_AcknowledgeConfig(i2c_name, DISABLE);
    //disableInterrupts();
    i++;
    p_data[i] = I2C_ReceiveData(i2c_name);
         while (!I2C_GetFlagStatus(i2c_name, I2C_FLAG_RXNE)) /* EV8 */
        {
        }
    i++;
    //I2C_GenerateSTOP(i2c_name, ENABLE);
    p_data[i] = I2C_ReceiveData(i2c_name);
    
    //enableInterrupts();
    
         while (!I2C_GetFlagStatus(i2c_name, I2C_FLAG_RXNE)) /* EV8 */
        {
        }
    
    i++;
    p_data[i] = I2C_ReceiveData(i2c_name);
    
    //I2C_AcknowledgeConfig(i2c_name, ENABLE);
    I2C_GenerateSTOP(i2c_name, ENABLE);
}
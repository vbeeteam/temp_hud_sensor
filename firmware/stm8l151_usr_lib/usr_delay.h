/**
 * ***********************************************************************************************
 * @file    : usr_delay.h
 * @brief   : header file of delay for STM8L from user
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
#ifndef USR_DELAY_H
#define USR_DELAY_H

/* Include -------------------------------------------------------------------------------------*/
#include "stdlib.h"
#include "stm8l15x.h"

/* Public Function -----------------------------------------------------------------------------*/
void delay_init(void);
void delay_isr(void);
void delay_ms(uint32_t value);

void TIM2_Config(void);
void delay_isr2(void);
void delay_ms_tx(uint32_t value);


#endif

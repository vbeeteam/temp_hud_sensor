#ifndef __M880A_HAL_H__
#define __M880A_HAL_H__

#include "platform.h"

#include <stdint.h>
#include <stdbool.h>

#include "stm8l15x.h"
#include "usr_gpio.h"


/*!
 *  SPI DEFINES
*/
#define SPI_NAME 									SPI1


/*!
 * SX127x definitions
 */

/*!
 * SX1272 RESET I/O definitions
 */
#define RESET_IOPORT                                LORA_RST_GPIO_PORT
#define RESET_PIN                                   LORA_RST_GPIO_PIN

/*!
 * SX1272 SPI NSS I/O definitions
 */
#define NSS_IOPORT                                  NSS_GPIO_PORT
#define NSS_PIN                                     NSS_GPIO_PIN

/*!
 * SX1272 DIO pins  I/O definitions
 */
#define DIO0_IOPORT                                 LORA_DIO_O_GPIO_PORT
#define DIO0_PIN                                    LORA_DIO_O_GPIO_PIN

#define DIO1_IOPORT                                 GPIOD
#define DIO1_PIN                                    GPIO_Pin_6

#define DIO2_IOPORT                                 GPIOD
#define DIO2_PIN                                    GPIO_Pin_6

#define DIO3_IOPORT                                 GPIOD
#define DIO3_PIN                                    GPIO_Pin_6

#define DIO4_IOPORT                                 GPIOD
#define DIO4_PIN                                    GPIO_Pin_6

#define DIO5_IOPORT                                 GPIOD
#define DIO5_PIN                                    GPIO_Pin_6

//FEM_CTX_PIN
#define RXTX_IOPORT                                 GPIOD
#define RXTX_PIN                                    GPIO_Pin_6



/*!
 * Initializes board peripherals
 */
void BoardInit( void );

#endif //__M880A_HAL_H__


/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: (1) "AS IS" WITH NO WARRANTY; AND 
 * (2)TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, SEMTECH SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) SEMTECH S.A.
 */
/*! 
 * \file       sx1276-Hal.c
 * \brief      SX1276 Hardware Abstraction Layer
 *
 * \version    2.0.B2 
 * \date       Nov 21 2012
 * \author     Miguel Luis
 *
 * Last modified by Miguel Luis on Jun 19 2013
 */
#include <stdint.h>
#include <stdbool.h> 

#include "platform.h"

#if defined( USE_SX1276_78_RADIO )

#include "usr_spi.h"
#include "usr_gpio.h"
#include "usr_delay.h"
#include "sx1276_78-Hal.h"
#include <string.h>


void TimeDelay(uint32_t value){
    delay_ms(value);
}

void SX1276SetReset( uint8_t state )
{
    if( state == RADIO_RESET_ON )
    {
        // Set Low level
        GPIO_ResetBits(RESET_IOPORT, RESET_PIN);
    }
    else
    {
        // set High level
		GPIO_SetBits(RESET_IOPORT, RESET_PIN);
    }
}

void SX1276Write( uint8_t addr, uint8_t data )
{
    SX1276WriteBuffer( addr, &data, 1 );
}

void SX1276Read( uint8_t addr, uint8_t *data )
{
    SX1276ReadBuffer( addr, data, 1 );
}

uint8_t debug_buff[255];

void SX1276WriteBuffer( uint8_t addr, uint8_t *buffer, uint8_t size )
{
		uint8_t i;
		uint8_t addr_send[1];
	
		uint8_t data_send[200];
		uint8_t data_recv[200];	
	
		for(i=0; i<size; i++)
		{
			data_send[i] = buffer[i];
		}	
    //memset(debug_buff, 0, 255);
    //NSS = 0;
    GPIO_ResetBits(NSS_IOPORT, NSS_PIN);

    addr_send[0] = addr | 0x80;
    spi_transmit_recv(SPI_NAME, addr_send, data_recv, 1);
    spi_transmit_recv(SPI_NAME, data_send, data_recv, size);

    //NSS = 1;
    GPIO_SetBits(NSS_IOPORT, NSS_PIN);
}

void SX1276ReadBuffer( uint8_t addr, uint8_t *buffer, uint8_t size )
{
    uint8_t i;
    uint8_t addr_send[1];
    uint8_t data_send[255];
    uint8_t data_recv[255];
    
    //memset(debug_buff, 0, 255);
    
    //NSS = 0;
    GPIO_ResetBits(NSS_IOPORT, NSS_PIN);

    addr_send[0] = addr & 0x7F;
    spi_transmit_recv(SPI_NAME, addr_send, data_recv, 1);
    for( i = 0; i < 255; i++ )
    {
        data_send[i] = 0;
    }
    spi_transmit_recv(SPI_NAME, data_send, data_recv, size);
    
    for(i=0; i<size; i++)
    {
        buffer[i] = data_recv[i];
    }

    //NSS = 1;
    GPIO_SetBits(NSS_IOPORT, NSS_PIN);
}

void SX1276WriteFifo( uint8_t *buffer, uint8_t size )
{
    SX1276WriteBuffer( 0, buffer, size );
}

void SX1276ReadFifo( uint8_t *buffer, uint8_t size )
{
    SX1276ReadBuffer( 0, buffer, size );
}

uint8_t debug_gpio = 100;

inline uint8_t SX1276ReadDio0( void )
{
    uint8_t result = 0;
    result = GPIO_ReadInputDataBit( DIO0_IOPORT, DIO0_PIN );
    debug_gpio = result;
    if(result == 0){
        return 0;
    }
    else{
        return 1;
    }
    //return GPIO_ReadInputDataBit( DIO0_IOPORT, DIO0_PIN );
}

inline uint8_t SX1276ReadDio1( void )
{
    return GPIO_ReadInputDataBit( DIO1_IOPORT, DIO1_PIN );
}

inline uint8_t SX1276ReadDio2( void )
{
    return GPIO_ReadInputDataBit( DIO2_IOPORT, DIO2_PIN );
}

inline uint8_t SX1276ReadDio3( void )
{
    return GPIO_ReadInputDataBit( DIO3_IOPORT, DIO3_PIN );
}

inline uint8_t SX1276ReadDio4( void )
{
    return GPIO_ReadInputDataBit( DIO4_IOPORT, DIO4_PIN );
}

inline uint8_t SX1276ReadDio5( void )
{
    return GPIO_ReadInputDataBit( DIO5_IOPORT, DIO5_PIN );
}

inline void SX1276WriteRxTx( uint8_t txEnable )
{
    if( txEnable != 0 )
    {
        GPIO_SetBits(RXTX_IOPORT, RXTX_PIN);
    }
    else
    {
        GPIO_ResetBits(RXTX_IOPORT, RXTX_PIN);
    }
}

#endif // USE_SX1276_78_RADIO

/**
 * ***********************************************************************************************
 * @file    : dev_process.c
 * @brief   : source file of process for device
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include --------------------------------------------------------------------------------------*/
#include "dev_process.h"
#include "string.h"
#include "dev_service.h"


/* Variable -------------------------------------------------------------------------------------*/
DevInforTypeDef dev_infor;

/* Function -------------------------------------------------------------------------------------*/
/**
 * @brief   : init process for device
 * @param   : None
 * @return  : None
**/
void dev_process_init(void){
    strncpy(dev_infor.dev_serial, "5pBbpk", 6); // device serial
    dev_infor.dev_type = TEMP_HUD_DEV_TYPE;
    dev_infor.op_period = 2000; // 2s
    dev_infor.time_start = 0;
    vbee_net_init();
}



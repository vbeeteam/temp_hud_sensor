/**
 * ***********************************************************************************************
 * @file    : usr_lowpower.c
 * @brief   : source file of User lib for low power mode
 * @author  : Tuan_NT
 * ***********************************************************************************************
**/
/* Include -------------------------------------------------------------------------------------*/
#include "usr_lowpower.h"
#include "usr_delay.h"

/* Variable -----------------------------------------------------------------------------------*/


/* Function -----------------------------------------------------------------------------------*/

/**
 * @brief   : init low power mode
 * @param   : none
 * @return  : none
**/
void lowpower_init(void){
    /* Enable RTC clock */
    CLK_RTCClockConfig(CLK_RTCCLKSource_LSE, CLK_RTCCLKDiv_1);
    /* Wait for LSE clock to be ready */
    while (CLK_GetFlagStatus(CLK_FLAG_LSERDY) == RESET);
    /* wait for 1 second for the LSE Stabilisation */
    delay_ms(1000);
    CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);

    /* Configures the RTC wakeup timer_step = RTCCLK/16 = LSE/16 = 488.28125 us */
    RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);

    /* Enable wake up unit Interrupt */
    RTC_ITConfig(RTC_IT_WUT, ENABLE);

    /* Enable general Interrupt*/
    enableInterrupts();
}


/**
 * @brief   : active low power mode
 * @param   : time for sleep // ms
 * @return  : none
**/
void lowpower_active(uint16_t time){
    RTC_SetWakeUpCounter(time);
  	RTC_WakeUpCmd(ENABLE);
    wfi();
}


/**
 * @brief   : exit low power mode
 * @param   : none
 * @return  : none
**/
void lowpower_exit(void){

}
